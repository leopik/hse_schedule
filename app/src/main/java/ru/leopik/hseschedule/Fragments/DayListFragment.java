package ru.leopik.hseschedule.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.joda.time.LocalDate;

import java.util.Map;

import ru.leopik.hseschedule.Activities.MainActivity;
import ru.leopik.hseschedule.Adapters.DayListAdapter;
import ru.leopik.hseschedule.DaySchedule;
import ru.leopik.hseschedule.EndlessRecyclerViewScrollListener;
import ru.leopik.hseschedule.LocalDateRange;
import ru.leopik.hseschedule.R;
import ru.leopik.hseschedule.ScheduleGetter;
import ru.leopik.hseschedule.ScheduleHandler;

import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_IDLE;

public class DayListFragment extends Fragment implements ScheduleHandler {
    // TODO: 13.08.2018 maybe add header to load previos days or at least remember why it's a bad idea
    // TODO: 14.07.2018 check why performance is low
    private RecyclerView daysList;
    private DayListAdapter dayListAdapter;
    private SwipeRefreshLayout swipeContainer;
    private LinearLayoutManager linearLayoutManager;
    private ScheduleGetter.ResultHandler resultHandler;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.day_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.daylist_title);

        //swipe to refresh part
        swipeContainer = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                MainActivity.getScheduleController().clearCache();

                dayListAdapter = new DayListAdapter(getActivity(), MainActivity.getScheduleController(), MainActivity.shownDate);
                daysList.setAdapter(dayListAdapter);

                //load first part
                MainActivity.getScheduleController().getDayRangeSchedule(
                        new LocalDateRange(
                                MainActivity.shownDate,
                                MainActivity.shownDate.plusDays(DayListAdapter.PERIOD_FOR_EXTEND)
                        ),
                        false,
                        resultHandler);
            }
        });

        daysList = (RecyclerView) getView().findViewById(R.id.day_list);
        linearLayoutManager = new LinearLayoutManager(getView().getContext());
        linearLayoutManager.setAutoMeasureEnabled(true);
        daysList.setLayoutManager(linearLayoutManager);

        resultHandler = new ScheduleGetter.ResultHandler() {
            @Override
            public void onComplete(Map<LocalDate, DaySchedule> results) {
                dayListAdapter.notifyDataSetChanged();
                swipeContainer.setRefreshing(false);
            }

            @Override
            public void onFail() {
                dayListAdapter.notifyDataSetChanged();
                Toast.makeText(getActivity(), getString(R.string.load_failure), Toast.LENGTH_SHORT).show();
                swipeContainer.setRefreshing(false);
            }
        };

        schedControllerChanged();//inits daylist adapter

        // generates schedule when scrolling
        daysList.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                dayListAdapter.listSize += DayListAdapter.PERIOD_FOR_EXTEND;
                MainActivity.getScheduleController().getDayRangeSchedule(
                        new LocalDateRange(
                                dayListAdapter.getStartDate().plusDays(totalItemsCount),
                                dayListAdapter.getStartDate().plusDays(totalItemsCount + DayListAdapter.PERIOD_FOR_EXTEND)
                        ),
                        false,
                        resultHandler);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == SCROLL_STATE_IDLE) {
                        MainActivity.shownDate = dayListAdapter.getStartDate().plusDays(linearLayoutManager.findFirstCompletelyVisibleItemPosition());

                }
            }
        });

        //FAB handler
        final FloatingActionButton fab = (FloatingActionButton) getView().findViewById(R.id.home_fab);
        fab.hide(new FloatingActionButton.OnVisibilityChangedListener() {
            @Override
            public void onHidden(FloatingActionButton fab) {
                super.onHidden(fab);
                fab.setVisibility(View.INVISIBLE);
            }
        }); //hide by default
        //todo fix this shit
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDate(new LocalDate());
                fab.hide(new FloatingActionButton.OnVisibilityChangedListener() {
                    @Override
                    public void onHidden(FloatingActionButton fab) {
                        super.onHidden(fab);
                        fab.setVisibility(View.INVISIBLE);
                    }
                });
            }
        });
    }

    @Override
    public void showDate(LocalDate date) {
        MainActivity.shownDate = date;
        schedControllerChanged();
    }

    @Override
    public void schedControllerChanged() {
        //hack to load some previous data too
        LocalDate realStartDate = MainActivity.shownDate.minusDays(DayListAdapter.PERIOD_FOR_EXTEND/2);
        dayListAdapter = new DayListAdapter(getActivity(), MainActivity.getScheduleController(), realStartDate);
        daysList.setAdapter(dayListAdapter);

        //load first part
        MainActivity.getScheduleController().getDayRangeSchedule(new LocalDateRange(realStartDate, realStartDate.plusDays(DayListAdapter.PERIOD_FOR_EXTEND)),
                false, resultHandler);

        //scroll to currdate
        daysList.scrollToPosition(DayListAdapter.PERIOD_FOR_EXTEND/2);
    }

    public static DayListFragment newInstance() {

        Bundle args = new Bundle();

        DayListFragment fragment = new DayListFragment();
        fragment.setArguments(args);
        return fragment;
    }

}
