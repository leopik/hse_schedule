package ru.leopik.hseschedule.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.joda.time.Days;
import org.joda.time.LocalDate;

import ru.leopik.hseschedule.Activities.MainActivity;
import ru.leopik.hseschedule.Adapters.CardPagerAdapter;
import ru.leopik.hseschedule.R;
import ru.leopik.hseschedule.ScheduleHandler;

// probably this should be done as https://pp.vk.me/c636027/v636027705/34b44/mhY4E9PJRs0.jpg

/**
 * Tabbed activity with days schedule
 */
public class TabbedDaysFragment extends android.app.Fragment implements ScheduleHandler {

    private ViewPager viewPager;
//    private SwipeRefreshLayout swipeContainer;

    // TODO: 14.07.2018 add curr date to fab maybe, and after testing add it here too
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tabbed_days_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.tabbeddays_title);

//        //swipe to refresh part
//        swipeContainer = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipeContainer_tabbed);
//        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                MainActivity.getScheduleController().clearCache();
//
//                schedControllerChanged();
//            }
//        });

        //FAB handler
        final FloatingActionButton fab = (FloatingActionButton) getView().findViewById(R.id.home_fab_tabbed);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDate(new LocalDate());
            }
        });

        viewPager = (ViewPager) getActivity().findViewById(R.id.tabbed_days_viewpager);
        viewPager.setOffscreenPageLimit(3);
        CardPagerAdapter cardPagerAdapter = new CardPagerAdapter(getActivity(), MainActivity.getScheduleController());
        viewPager.setAdapter(cardPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                MainActivity.shownDate = new LocalDate().plusDays(position - CardPagerAdapter.CENTER_OFFSET);
                if (MainActivity.shownDate.equals(new LocalDate())) {
                    fab.hide();
                } else {
                    fab.show();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        //todo maybe inherit viewpager and override setCurrent item in order to avoid shitty date operations
        //scroll to last saved pos
        viewPager.setCurrentItem(CardPagerAdapter.CENTER_OFFSET + Days.daysBetween(new LocalDate(), MainActivity.shownDate).getDays());
    }

    @Override
    public void schedControllerChanged() {
        int openedItem = viewPager.getCurrentItem();
        viewPager.setAdapter(new CardPagerAdapter(getActivity(), MainActivity.getScheduleController()));
        viewPager.setCurrentItem(openedItem);
    }

    @Override
    public void showDate(LocalDate date) {
        Days daysBetween = Days.daysBetween(new LocalDate(), date);
        int scrollOffset = daysBetween.getDays();
        viewPager.setCurrentItem(CardPagerAdapter.CENTER_OFFSET + scrollOffset);
    }

    public static TabbedDaysFragment newInstance() {

        Bundle args = new Bundle();

        TabbedDaysFragment fragment = new TabbedDaysFragment();
        fragment.setArguments(args);
        return fragment;
    }

}
