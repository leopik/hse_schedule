package ru.leopik.hseschedule.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Set;
import java.util.TreeSet;

import ru.leopik.hseschedule.Activities.MainActivity;
import ru.leopik.hseschedule.R;

public class SettingsFragment extends android.preference.PreferenceFragment implements TimePickerDialog.OnTimeSetListener {

    public final static String ALARM_OFFSET_PREFERENCE_PREF = "alarm_offset";
    public final static String LONG_CLICK_SETS_ALARM = "long_click_alarm";
    public final static String EMAIL_ADDRESS_PREF = "email";
    public final static String SCHED_LANGUAGE_PREF = "language_pref";

    @Override
    public void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        getActivity().setTitle(getString(R.string.menu_item_settings_title));

        //alarm offset pref
        Preference alarmOffset = findPreference(ALARM_OFFSET_PREFERENCE_PREF);
        alarmOffset.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference preference) {
                showTimePickerDialog();
                return true;
            }
        });

        //clear cache pref
        Preference clearCache = findPreference(getString(R.string.clear_email_cahec_pref));
        clearCache.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Set<String> emailCache = PreferenceManager
                        .getDefaultSharedPreferences(getActivity())
                        .getStringSet(MainActivity.HISTORY_EMAIL_LIST_PREF, new TreeSet<String>());
                emailCache.clear();
                Toast.makeText(getActivity(), getString(R.string.email_cache_cleared), Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        //default email pref
        final EditTextPreference userEmail = (EditTextPreference) findPreference(EMAIL_ADDRESS_PREF);
        userEmail.getEditText().addTextChangedListener(
                new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
//                        Dialog a = userEmail.getDialog();
                        AlertDialog emailDialog = (AlertDialog) userEmail.getDialog();
                        if (emailDialog != null) {
                            if (!isEmailValid(userEmail.getEditText().getText().toString())) {
                                userEmail.getEditText().setError(getResources().getString(R.string.invalid_email_hint));

                                emailDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
                            } else {
                                userEmail.getEditText().setError(null);
                                emailDialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);
                            }
                        }
                    }

                    boolean isEmailValid(CharSequence email) {
                        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
                    }
                });
        userEmail.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                ((MainActivity) getActivity()).changeEmail((String) newValue);
                return true;
            }
        });

        //sched lang pref
        Preference schedLang = findPreference(SCHED_LANGUAGE_PREF);

    }

    private void showTimePickerDialog(){
        int alarmOffset = PreferenceManager.getDefaultSharedPreferences(this.getActivity())
                .getInt(ALARM_OFFSET_PREFERENCE_PREF, 60);
        new TimePickerDialog(getActivity(),
                this,
                alarmOffset/60,
                alarmOffset%60,
                true)
                .show();
    }

    public static SettingsFragment newInstance() {

        Bundle args = new Bundle();

        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        preferences.edit().putInt(ALARM_OFFSET_PREFERENCE_PREF, hourOfDay*60 + minute).apply();
    }
}
