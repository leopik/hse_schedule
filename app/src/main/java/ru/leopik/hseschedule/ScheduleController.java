package ru.leopik.hseschedule;

import org.joda.time.LocalDate;

import java.util.Map;
import java.util.TreeMap;

//controls schedule getting, giving away and caching
public class ScheduleController {

    private TreeMap<LocalDate, DaySchedule> daysCache = new TreeMap<>();
    private String email;
    private int languageCode;
    private String language;
    private final static int LOAD_THRESHOLD = 3;

    public String getEmail() {
        return email;
    }

    public String getLanguage() {
        return language;
    }

    public ScheduleController(String email, String language) {
        this.email = email;
        this.language = language;
        if (language.equals("Russian"))
            languageCode = 1;
        else
            languageCode = 2;
    }

    //todo maybe getDayschedule should return cached day and give updated day inside handler
    public DaySchedule getLocalDaySchedule(LocalDate day) {
        if (daysCache.containsKey(day))
            return  daysCache.get(day);
        return null;
    }

    // TODO: 08.05.2018 find cahce libs max save time
    public void getDaySchedule(final LocalDate day, boolean forceReload, final ScheduleGetter.ResultHandler handler) {
        if (!forceReload && daysCache.containsKey(day)) {
            TreeMap<LocalDate, DaySchedule> daySched = new TreeMap<>();
            daySched.put(day, daysCache.get(day));
            handler.onComplete(daySched);
            // TODO: 09.05.2018 check whehere days within threshold are already cached - if not - load ones not cached. Must be function to detect least neccessary intersect with range in cache
        } else {
            getSchedule(day.minusDays(LOAD_THRESHOLD), day.plusDays(LOAD_THRESHOLD), handler);
        }
    }

    public void getDayRangeSchedule(final LocalDateRange daysRange, boolean forceReload, final ScheduleGetter.ResultHandler handler) {
        if (forceReload) {
            getSchedule(daysRange, handler);
        } else {
            boolean cached = true;
            for (LocalDate date : daysRange)
                if (!daysCache.containsKey(date))
                    cached = false;

            if (cached) {
                handler.onComplete(daysCache.subMap(daysRange.getStart(), true, daysRange.getEnd(), true));
            } else {
                getSchedule(daysRange, handler);
            }
        }
    }

    private void getSchedule(LocalDateRange range, ScheduleGetter.ResultHandler handler) {
        if (range == null)
            return;
        getSchedule(range.getStart(), range.getEnd(), handler);
    }

    private void getSchedule(final LocalDate startDate, final LocalDate endDate, final ScheduleGetter.ResultHandler handler) {
        new ScheduleGetter(email, startDate, endDate,  languageCode, new ScheduleGetter.ResultHandler() {
            @Override
            public void onComplete(Map<LocalDate, DaySchedule> results) {
                Map<LocalDate, DaySchedule> mergeMap = new TreeMap<>();
                for (LocalDate date : new LocalDateRange(startDate,endDate)) {
                    if (results.containsKey(date)) {
                        mergeMap.put(date, results.get(date));
                    } else {
                        //mergeMap.put(date, new DaySchedule(date));
                        mergeMap.put(date, null);
                    }
                }

                daysCache.putAll(mergeMap);
                handler.onComplete(results);
            }

            @Override
            public void onFail() {
                handler.onFail();
            }
        }).execute();
    }

    // TODO: 09.05.2018 create best overlap detection alght
    private LocalDateRange bestDayRangeToLoad(LocalDateRange rangeToExtend) {
        return rangeToExtend;
    }


    public void clearCache() {
        daysCache.clear();
    }
}
