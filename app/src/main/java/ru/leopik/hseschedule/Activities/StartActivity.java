package ru.leopik.hseschedule.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ru.leopik.hseschedule.R;

import static ru.leopik.hseschedule.Fragments.SettingsFragment.EMAIL_ADDRESS_PREF;

/**
 * Checks whether email is known, if not - launches login screen,
 * else - shows schedule for known email
 */
public class StartActivity extends AppCompatActivity {


    private final static String TAG = "StartActivity class";
    private SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // check whether email is saved in preferences
        PreferenceManager.setDefaultValues(this, R.xml.settings, false);
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        if (settings.contains(EMAIL_ADDRESS_PREF)) {
            startMainActivity();
        } else {
            setContentView(R.layout.activity_login);
            initializeUI();
        }
    }

    /**
     * This method is used to initialize UI Components
     */
    private void initializeUI(){
        final EditText emailEditText = (EditText) findViewById(R.id.emailEditText);
        final Button loginButton = (Button) findViewById(R.id.loginButton);

        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // saves email from edittext field
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(EMAIL_ADDRESS_PREF, emailEditText.getText().toString());
                editor.apply();

                startMainActivity();
            }
        });

        emailEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                checkEmail(emailEditText);
            }

            void checkEmail(EditText edt) {
                if (!isEmailValid(edt.getText().toString())) {
                    edt.setError(getResources().getString(R.string.invalid_email_hint));
                } else {
                    loginButton.setEnabled(true);
                }
            }

            boolean isEmailValid(CharSequence email) {
                return Patterns.EMAIL_ADDRESS.matcher(email).matches();
            }
        });
    }

    /**
     * Starts schedule and closes this activity
     */
    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
