package ru.leopik.hseschedule.Activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import ru.leopik.hseschedule.Fragments.DayListFragment;
import ru.leopik.hseschedule.Fragments.SettingsFragment;
import ru.leopik.hseschedule.Fragments.TabbedDaysFragment;
import ru.leopik.hseschedule.R;
import ru.leopik.hseschedule.ScheduleController;
import ru.leopik.hseschedule.ScheduleHandler;
// SEEMS COMPLETE

/**
 * Main class which holds all fragments and works with navdrawer
 */
public class MainActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener{

    public final static String HMMMMM = "https://memepedia.ru/wp-content/uploads/2018/06/kto-prochital-tot-sdohnet.jpg";

    private final static String DEFAULT_FRAGMENT_PREF = "default_fragment";
    public final static String HISTORY_EMAIL_LIST_PREF = "history list";
    private final static String LAST_SHOWN_DATE_PREF = "last shown date";

    public final static int PLACEHOLDER_ID = R.id.placeholder_layout;

    public static LocalDate shownDate;
    private SharedPreferences settings;
    private static ScheduleController scheduleController;

    public static ScheduleController getScheduleController() {
        return scheduleController;
    }

    private Fragment currentFragment;
    private AutoCompleteTextView emailField;
    private DrawerLayout drawer;
    private TextView navDrawerEmailText;
    private boolean navBarFragmentChosen = false; // in order to not open same fragment many times

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // handling navbar and toolbar
        drawer = (DrawerLayout) findViewById(R.id.drawer_activity_container);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                //this was check whether close drawer and only after that replace fragment is better. No
                //https://gist.github.com/cyrilpillai/e357c47e76381db0b34b4a1c60d1a819
//                if (navBarFragmentChosen) {
//                    navBarFragmentChosen = false;
//                    if (currentFragment != null) {
//                        getFragmentManager().beginTransaction().replace(PLACEHOLDER_ID, currentFragment).commit();
//                    }
//                }
            }
        };
        drawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        NavigationView navView = (NavigationView) findViewById(R.id.navigation_view);
        navView.setNavigationItemSelectedListener(this);
        navDrawerEmailText = (TextView) navView.getHeaderView(0).findViewById(R.id.header_user_email);

        // getting user's email from prefs
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        String userEmail = settings.getString(SettingsFragment.EMAIL_ADDRESS_PREF, null);
        String schedLang = settings.getString(SettingsFragment.SCHED_LANGUAGE_PREF, "English");
        String launchFragmentName = settings.getString(DEFAULT_FRAGMENT_PREF, "");

        //getting date at which user stopped app
        shownDate = new LocalDate(settings.getString(LAST_SHOWN_DATE_PREF, new LocalDate().toString()));

        //schedule controller creation and launching default fragment
        scheduleController = new ScheduleController(userEmail, schedLang);
        setNavDrawerEmail(scheduleController.getEmail());

        switch (launchFragmentName) {
            case "TabbedDaysFragment":
                currentFragment = TabbedDaysFragment.newInstance();
                navView.setCheckedItem(R.id.menu_item_tabbed_days_activity);
                break;

            default:
                currentFragment = DayListFragment.newInstance();
                navView.setCheckedItem(R.id.menu_item_daylist_activity);
                break;
        }
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(PLACEHOLDER_ID, currentFragment);
        ft.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (!item.isChecked()) {
            int id = item.getItemId();
            Fragment fragment = null;
            switch (id) {
                case R.id.menu_item_daylist_activity:
                    fragment = DayListFragment.newInstance();
                    break;

                case R.id.menu_item_tabbed_days_activity:
                    fragment = TabbedDaysFragment.newInstance();
                    break;

                case R.id.menu_item_settings:
                    fragment = SettingsFragment.newInstance();
                    break;

                case R.id.menu_item_custom_email:
                    showEmailDialog();
                    break;

                case R.id.menu_item_date_set:
                    showDatePickerDialog();
                    break;

                case R.id.about_author_button:
                    showAboutAuthorDialog();
                    break;
            }

            if (fragment != null) {
                getFragmentManager().beginTransaction().replace(PLACEHOLDER_ID, fragment).commit();
//                navBarFragmentChosen = true;
                currentFragment = fragment;
            }
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // writes current email in navdrawer header
    private void setNavDrawerEmail(String newEmail) {
        Resources res = getResources();
        String headerEmailText = res.getString(R.string.header_user_email_text, newEmail);
        navDrawerEmailText.setText(headerEmailText);
    }

    private void showDatePickerDialog() {
        LocalDate tempDate = new LocalDate();
        new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month++; //month in datePickedDialog starts with 0, year and dayOfMonth start with 1 WTF?

                        if (currentFragment instanceof ScheduleHandler) {
                            ((ScheduleHandler) currentFragment).showDate(new LocalDate(year, month, dayOfMonth));
                        }
                    }
                },
                tempDate.getYear(),
                tempDate.getMonthOfYear() - 1, //month - 1 is because jodatime and android have different first month number (0/1)
                tempDate.getDayOfMonth())
                .show();
    }

    private void showEmailDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.custom_email_dialog_message);
        builder.setView(getLayoutInflater().inflate(R.layout.custom_email_dialog, null));

        builder.setPositiveButton(R.string.dialog_done_button_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String email = emailField.getText().toString();
                changeEmail(email);
                Set<String> emailSet = settings.getStringSet(HISTORY_EMAIL_LIST_PREF, new TreeSet<String>());
                emailSet.add(email);
                settings.edit().putStringSet(HISTORY_EMAIL_LIST_PREF, emailSet).apply();
            }
        });

        builder.setNegativeButton(R.string.dialog_cancel_button_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.setNeutralButton(R.string.dialog_neutral_button_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String email = settings.getString(SettingsFragment.EMAIL_ADDRESS_PREF, null);
                changeEmail(email);
            }
        });

        final AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);

        emailField = (AutoCompleteTextView) dialog.findViewById(R.id.custom_email_edittext);
        ArrayList<String> emailList = new ArrayList<>(settings.getStringSet(HISTORY_EMAIL_LIST_PREF, new TreeSet<String>()));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, emailList);
        emailField.setAdapter(adapter);
        emailField.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                checkEmail(emailField);
            }

            void checkEmail(EditText edt) {
                if (!isEmailValid(edt.getText().toString())) {
                    edt.setError(getResources().getString(R.string.invalid_email_hint));
                    dialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(false);
                } else {
                    edt.setError(null);
                    dialog.getButton(Dialog.BUTTON_POSITIVE).setEnabled(true);
                }
            }

            boolean isEmailValid(CharSequence email) {
                return Patterns.EMAIL_ADDRESS.matcher(email).matches();
            }
        });
    }

    private void showAboutAuthorDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.author_text);
        builder.setView(getLayoutInflater().inflate(R.layout.about_author_dialog, null));
        builder.create().show();
    }

    public void changeEmail(String email) {
        if (email.equals(scheduleController.getEmail()))
            return;
        String toastMessage = getResources().getString(R.string.email_change_toast, email);
        Toast.makeText(this, toastMessage, Toast.LENGTH_SHORT).show();
        scheduleController = new ScheduleController(email, scheduleController.getLanguage());
        setNavDrawerEmail(email);
        if (currentFragment instanceof ScheduleHandler) {
            ((ScheduleHandler) currentFragment).schedControllerChanged();
        }
    }

    public void changeLang(String lang) {
        if (lang.equals(scheduleController.getLanguage()))
            return;
        scheduleController = new ScheduleController(scheduleController.getEmail(), lang);

        if (currentFragment instanceof ScheduleHandler) {
            ((ScheduleHandler) currentFragment).schedControllerChanged();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        //settings.edit().putLong(LAST_SHOWN_DATE_PREF, shownDate.toDateTimeAtStartOfDay().getMillisOfDay()).apply();
        settings.edit().putString(LAST_SHOWN_DATE_PREF, shownDate.toString()).apply();
    }
}
