package ru.leopik.hseschedule;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import ru.leopik.hseschedule.Deserializers.LocalDateDeserializer;
import ru.leopik.hseschedule.Deserializers.LocalTimeDeserializer;

/**
 * Gets schedule as JSON and works with
 */
public class ScheduleGetter extends AsyncTask<Void, Void, String> {

    private final static String TAG = "ScheduleGetter class";
    private LocalDate startDate, endDate;
    private int languageCode = 0;

    private final static String baseLink =
            "http://92.242.58.221/ruzservice.svc/v2/personlessons?";
    private String emailAdress;
    private String dateFormatPattern = LocalDateDeserializer.getDateFormatPattern();
    private ResultHandler handler;
    private final static String LANGUAGE_PREF = "language_pref";

    /**
     * Constructor for getting schedule by email address
     *
     * @param email email of person, whose schedule user wants to get
     * @param startDate first date of schedule including itself
     * @param endDate last date of schedule including itself
     */
    public ScheduleGetter(String email, LocalDate startDate, LocalDate endDate, int language, ResultHandler handler) {
        super();
        this.startDate = startDate;
        this.endDate = endDate;
        emailAdress = email;
        this.handler = handler;
        languageCode = language;
    }

    /**
     * Forms URL link to get schedule
     *
     * @return html adress of json schedule
     * @param baseLink
     */
    private String formRequest(String baseLink) {
        DateTimeFormatter dateFormat = DateTimeFormat.forPattern(dateFormatPattern);

        String startDateString = dateFormat.print(startDate);
        String endDateString = dateFormat.print(endDate);

        int receiverType = emailAdress.contains("edu.hse.ru")? 0 : 1;

        String html = String.format(baseLink + "fromdate=%1$s&todate=%2$s&receivertype=%3$s&email=%4$s&language=%5$d",
                startDateString, endDateString, receiverType, emailAdress, languageCode);
        return html;
    }

    /**
     * Deserializes json to array of subjects
     *
     * @param jsonString json string from which get subjects
     * @return array containing every subject from json (! unordered by date subjects)
     */
    private DaySchedule.Subject[] deserializeJson(String jsonString) {
        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, new LocalDateDeserializer())
                .registerTypeAdapter(LocalTime.class, new LocalTimeDeserializer()).create();

        JsonObject tempJsonObj = gson.fromJson(jsonString, JsonObject.class);
        JsonElement lessonsJsonArray = tempJsonObj.getAsJsonArray("Lessons");
        lessonsJsonArray.toString();
        DaySchedule.Subject[] subjects = gson.fromJson(lessonsJsonArray, DaySchedule.Subject[].class);
        return subjects;
    }

    /**
     * Gets schedule in form of JSON by given URL
     *
     * @param html url from which get json schedule
     * @return json string with schedule
     */
    private String getSchedule(String html) {
        HttpURLConnection urlConnection = null;
        BufferedReader bufferedReader = null;

        String result = "";
        try {
            URL url = new URL(html);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setConnectTimeout(30*1000);
            urlConnection.setReadTimeout(30*1000);

            int responseCode = urlConnection.getResponseCode();

            if (responseCode == 200) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";

                while ((line = bufferedReader.readLine()) != null) {
                    result += line;
                }

            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.e(TAG, "Malformed URL");
            result = "";
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "IO");
            result = "";
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(TAG, "IO");
                    result = "";
                }
            }

            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return result;
    }

    /**
     * Converts unordered by date subjects into structured list of DaySchedule
     * where all subjects with same date are packed in one DaySchedule object
     *
     * @param subjects
     * @return ordered by date days list
     */
    private TreeMap<LocalDate, DaySchedule> splitSubjectsIntoDays(DaySchedule.Subject[] subjects){
        TreeMap<LocalDate, DaySchedule> map = new TreeMap<>(new Comparator<LocalDate>() {
            @Override
            public int compare(LocalDate o1, LocalDate o2) {
                return o1.compareTo(o2);
            }
        });
        for (DaySchedule.Subject subj: subjects ) {
            LocalDate key = subj.getDate();

            if (!map.containsKey(key))
                map.put(key, new DaySchedule(key));
            map.get(key).addSubject(subj);
        }

        //ArrayList<DaySchedule> days = new ArrayList<>(map.values());

        return map;
    }

    @Override
    protected String doInBackground(Void... params) {
        String formedLink = formRequest(baseLink);
        String jsonResult = getSchedule(formedLink);
        if (jsonResult.equals("")) {
            return null;
        }
        return jsonResult;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result == null) {
            handler.onFail();
            return;
        }
        DaySchedule.Subject[] subjects;
        try {
            subjects = deserializeJson(result);
        } catch (JsonSyntaxException ex) {
            handler.onFail();
            return;
        }
        TreeMap<LocalDate, DaySchedule> dayScheduleList = splitSubjectsIntoDays(subjects);

        handler.onComplete(dayScheduleList);
    }

    public interface ResultHandler {
        void onComplete(Map<LocalDate, DaySchedule> results);
        void onFail();
    }
}