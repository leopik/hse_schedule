package ru.leopik.hseschedule;

import android.os.Parcel;
import android.os.Parcelable;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Class which contains DaySchedule and Subject model
 */
public class DaySchedule {

    public DaySchedule(LocalDate dayDate) {
        this.dayDate = dayDate;
        subjects = new ArrayList<>();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof DaySchedule) {
            return ((DaySchedule) obj).getLocalDate().equals(dayDate);
        }
        return false;
    }

    public String getHeaderFormattedDate() {
        return getHeaderFormattedDate(dayDate);
    }

    public static String getHeaderFormattedDate(LocalDate date) {
        String headerDate;
        DateTimeFormatter format = DateTimeFormat.forPattern("EEEE, d.MM.yyyy").withLocale(Locale.getDefault());
        headerDate = format.print(date);
        // for some reason first letter of dayname is not uppercase, this line corrects the mistake
        headerDate = headerDate.substring(0,1).toUpperCase() + headerDate.substring(1).toLowerCase();
        return headerDate;
    }

    public void fillSubjectList() {
        for (int i=0; i<6; i++) {
            subjects.add(new Subject("kekich" + i));
        }
    }

    public DaySchedule(LocalDate dayDate, Subject[] subjects) {
        this.dayDate = dayDate;
        this.subjects = Arrays.asList(subjects);
    }

    private LocalDate dayDate;
    private List<Subject> subjects;

    public LocalDate getLocalDate() {
        return dayDate;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void addSubject(Subject subject) {
        subjects.add(subject);
    }

    public static class Subject implements Parcelable {

        Subject() {
        } // empty constructor for Json filling

        // just for test
        Subject(String name) {
            discipline = name;
            auditorium = "auditorium";
            beginLesson = new LocalTime();
            building = "building";
            date = new LocalDate();
            dayOfWeek = 1;
            endLesson = new LocalTime();
            group = "group";
            kindOfWork = "kindofwork";
            lecturer = "lecturer";
            stream = "stream";
            subGroup = "subGroup";
        }

        public static final String formatPattern = "HH:mm";

        @Override
        public String toString() {
            return "Auditorium: " + auditorium + "; Begin lesson: " + beginLesson + "; End lesson: " +
                    endLesson + "; Building: " + building + "; Date:" + date + "; Day of week: " +
                    dayOfWeek + "; Discipline" + discipline + "; Group: " + group + "; Kind of work: " +
                    kindOfWork + "; Lecturer: " + lecturer + "; Stream: " + stream + "; Subgroup: " + subGroup;
        }

        private String auditorium;
        private int auditoriumId;
        private LocalTime beginLesson;
        private String building;
        private LocalDate date;
        private int dayOfWeek;
        private String discipline;
        private LocalTime endLesson;
        private String group;
        private int group0id;
        private String kindOfWork;
        private String lecturer;
        private int lecturer0id;
        private String stream;
        private int stream0id;
        private String subGroup;
        private int subGroup0id;

        public String getAuditorium() {
            return auditorium;
        }

        public int getAuditoriumId() {
            return auditoriumId;
        }

        public LocalTime getBeginLesson() {
            return beginLesson;
        }

        public String getBeginLessonString() {
            return DateTimeFormat.forPattern(formatPattern).print(beginLesson);
        }

        public String getBuilding() {
            return building;
        }

        public LocalDate getDate() {
            return date;
        }

        public int getDayOfWeek() {
            return dayOfWeek;
        }

        public String getDiscipline() {
            return discipline;
        }

        public LocalTime getEndLesson() {
            return endLesson;
        }

        public String getEndLessonString() {
            return DateTimeFormat.forPattern(formatPattern).print(endLesson);
        }

        public String getGroup() {
            return group;
        }

        public int getGroup0id() {
            return group0id;
        }

        public String getKindOfWork() {
            return kindOfWork;
        }

        public String getLecturer() {
            return lecturer;
        }

        public int getLecturer0id() {
            return lecturer0id;
        }

        public String getStream() {
            return stream;
        }

        public int getStream0id() {
            return stream0id;
        }

        public String getSubGroup() {
            return subGroup;
        }

        public int getSubGroup0id() {
            return subGroup0id;
        }

        protected Subject(Parcel in) {
            auditorium = in.readString();
            auditoriumId = in.readInt();
            beginLesson = (LocalTime) in.readValue(LocalTime.class.getClassLoader());
            building = in.readString();
            date = (LocalDate) in.readValue(LocalDate.class.getClassLoader());
            dayOfWeek = in.readInt();
            discipline = in.readString();
            endLesson = (LocalTime) in.readValue(LocalTime.class.getClassLoader());
            group = in.readString();
            group0id = in.readInt();
            kindOfWork = in.readString();
            lecturer = in.readString();
            lecturer0id = in.readInt();
            stream = in.readString();
            stream0id = in.readInt();
            subGroup = in.readString();
            subGroup0id = in.readInt();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(auditorium);
            dest.writeInt(auditoriumId);
            dest.writeValue(beginLesson);
            dest.writeString(building);
            dest.writeValue(date);
            dest.writeInt(dayOfWeek);
            dest.writeString(discipline);
            dest.writeValue(endLesson);
            dest.writeString(group);
            dest.writeInt(group0id);
            dest.writeString(kindOfWork);
            dest.writeString(lecturer);
            dest.writeInt(lecturer0id);
            dest.writeString(stream);
            dest.writeInt(stream0id);
            dest.writeString(subGroup);
            dest.writeInt(subGroup0id);
        }

        @SuppressWarnings("unused")
        public static final Creator<Subject> CREATOR = new Creator<Subject>() {
            @Override
            public Subject createFromParcel(Parcel in) {
                return new Subject(in);
            }

            @Override
            public Subject[] newArray(int size) {
                return new Subject[size];
            }
        };
    }
}
