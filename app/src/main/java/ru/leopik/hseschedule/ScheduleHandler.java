package ru.leopik.hseschedule;

import org.joda.time.LocalDate;

/**
 * Created by Leopik on 19.01.2017.
 */

public interface ScheduleHandler {

    void showDate(LocalDate date);
    void schedControllerChanged();
}
