package ru.leopik.hseschedule.Deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Type;

/**
 * Used for deserializing Json date to LocalDate
 */
public class LocalTimeDeserializer implements JsonDeserializer<LocalTime> {

    private final static String timeFormatPattern = "HH:mm";

    final DateTimeFormatter fmt = DateTimeFormat.forPattern(timeFormatPattern);

    @Override
    public LocalTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return LocalTime.parse(json.getAsString(), fmt);
    }
}
