package ru.leopik.hseschedule.Deserializers;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Type;

/**
 * Used for deserializing Json date to LocalDate
 */
public class LocalDateDeserializer implements JsonDeserializer<LocalDate> {

    private final static String dateFormatPattern = "yyyy.MM.dd";
    public static String getDateFormatPattern() {
        return dateFormatPattern;
    }

    final DateTimeFormatter fmt = DateTimeFormat.forPattern(dateFormatPattern);

    @Override
    public LocalDate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return LocalDate.parse(json.getAsString(), fmt);
    }
}
