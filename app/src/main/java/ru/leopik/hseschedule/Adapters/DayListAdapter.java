package ru.leopik.hseschedule.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.joda.time.LocalDate;

import ru.leopik.hseschedule.DaySchedule;
import ru.leopik.hseschedule.R;
import ru.leopik.hseschedule.ScheduleController;
import ru.leopik.hseschedule.SimpleDividerItemDecoration;

public class DayListAdapter extends RecyclerView.Adapter<DayListAdapter.DayViewHolder>   {

    private Context context;
    private ScheduleController scheduleController;
    private LocalDate startDate;

    public LocalDate getStartDate() {
        return startDate;
    }

    public static final int PERIOD_FOR_EXTEND = 28;
    public int listSize = PERIOD_FOR_EXTEND;

    public DayListAdapter(Context context, ScheduleController scheduleController, LocalDate startDate) {
        this.context = context;
        this.scheduleController = scheduleController;
        this.startDate = startDate;
    }

    @Override
    public DayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View dayView = layoutInflater.inflate(R.layout.single_day, parent, false);
        DayViewHolder viewHolder = new DayViewHolder(dayView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final DayViewHolder holder, int position) {
        final LocalDate shownDate = startDate.plusDays(position);

        holder.header.setText(DaySchedule.getHeaderFormattedDate(shownDate));
        DaySchedule daySchedule = scheduleController.getLocalDaySchedule(shownDate);
        if (daySchedule != null) {
            holder.emptyDayText.setVisibility(View.INVISIBLE);
        } else {
            holder.emptyDayText.setVisibility(View.VISIBLE);

            //placeholder in order to avoid "No adapter attached; skipping layout" error
            daySchedule = new DaySchedule(shownDate);
        }
        holder.subjectList.setAdapter(new SubjectListAdapter(context, daySchedule));

    }

    @Override
    public int getItemCount() {
        return listSize;
    }


    static class DayViewHolder extends RecyclerView.ViewHolder {

        private RecyclerView subjectList;
        //private CardView dayCard;
        private TextView header;
        private TextView emptyDayText;

        public DayViewHolder(View itemView) {
            super(itemView);

            subjectList = (RecyclerView) itemView.findViewById(R.id.subject_list);
            //dayCard = (CardView) itemView.findViewById(R.id.single_day_card);
            header = (TextView) itemView.findViewById(R.id.card_header);
            emptyDayText = (TextView) itemView.findViewById(R.id.empty_day);

            subjectList.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
            subjectList.addItemDecoration(new SimpleDividerItemDecoration(itemView.getContext()));
            subjectList.setNestedScrollingEnabled(false);

        }
    }
}
