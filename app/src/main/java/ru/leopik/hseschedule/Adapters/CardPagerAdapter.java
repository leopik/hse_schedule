package ru.leopik.hseschedule.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.LocalDate;

import java.util.Map;

import ru.leopik.hseschedule.DaySchedule;
import ru.leopik.hseschedule.R;
import ru.leopik.hseschedule.ScheduleController;
import ru.leopik.hseschedule.ScheduleGetter;
import ru.leopik.hseschedule.SimpleDividerItemDecoration;

/**
 * Created by Leopik on 06.05.2017.
 */

public class CardPagerAdapter extends PagerAdapter {

    public static final int CENTER_OFFSET = 1000;
    private LocalDate currentDate = new LocalDate();
    private Context context;
    private ScheduleController scheduleController;
//    private SwipeRefreshLayout swipeContainer;

    public CardPagerAdapter(Context context, ScheduleController scheduleController) {
        this.context = context;
        this.scheduleController = scheduleController;
//        this.swipeContainer = swipeContainer;
    }

    @Override
    public int getCount() {
        return CENTER_OFFSET*2;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position){
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ViewGroup itemView = (ViewGroup) layoutInflater.inflate(R.layout.single_day, container, false);

        //CardView cardView = (CardView) itemView.findViewById(R.id.single_day_card);
        TextView header = (TextView) itemView.findViewById(R.id.card_header);
        final RecyclerView subjectList = (RecyclerView) itemView.findViewById(R.id.subject_list);
        final TextView emptyDayText = (TextView) itemView.findViewById(R.id.empty_day);

        subjectList.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
        subjectList.addItemDecoration(new SimpleDividerItemDecoration(itemView.getContext()));

        final LocalDate shownDate = currentDate.plusDays(position - CENTER_OFFSET);

        //placeholder in oreder to avoid "No adapter attached; skipping layout" error
        DaySchedule tempDaySchedule = new DaySchedule(shownDate);
        SubjectListAdapter placeholderAdapter = new SubjectListAdapter(context, tempDaySchedule);
        subjectList.setAdapter(placeholderAdapter);

        emptyDayText.setVisibility(View.VISIBLE);

        header.setText(DaySchedule.getHeaderFormattedDate(shownDate));
        container.addView(itemView);

        scheduleController.getDaySchedule(shownDate, false, new ScheduleGetter.ResultHandler() {
            @Override
            public void onComplete(Map<LocalDate, DaySchedule> results) {
                if (results.containsKey(shownDate) && results.get(shownDate) != null) {
                    emptyDayText.setVisibility(View.INVISIBLE);
                    subjectList.setAdapter(new SubjectListAdapter(context, results.get(shownDate)));
                } else {
                    emptyDayText.setVisibility(View.VISIBLE);
                }
//                swipeContainer.setRefreshing(false);
            }

            @Override
            public void onFail() {
                Toast.makeText(context, context.getString(R.string.load_failure), Toast.LENGTH_SHORT).show();
//                swipeContainer.setRefreshing(false);
            }
        });
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object view){
        container.removeView((View) view);
    }
}
