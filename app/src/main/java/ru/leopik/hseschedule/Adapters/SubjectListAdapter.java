package ru.leopik.hseschedule.Adapters;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.provider.AlarmClock;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import org.joda.time.LocalTime;

import ru.leopik.hseschedule.DaySchedule;
import ru.leopik.hseschedule.R;

import static ru.leopik.hseschedule.Fragments.SettingsFragment.ALARM_OFFSET_PREFERENCE_PREF;
import static ru.leopik.hseschedule.Fragments.SettingsFragment.LONG_CLICK_SETS_ALARM;

/**
 * Created by Leopik on 20.11.2016.
 */

public class SubjectListAdapter extends RecyclerView.Adapter<SubjectListAdapter.SubjectViewHolder>{

    private DaySchedule day;
    private Context context;

    public SubjectListAdapter(Context context, DaySchedule daySchedule) {
        this.context = context;
        day = daySchedule;
    }

    @Override
    public SubjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View subjectView = layoutInflater.inflate(R.layout.single_subject_item, parent, false);
        SubjectViewHolder viewHolder = new SubjectViewHolder(subjectView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SubjectViewHolder holder, int position) {
        DaySchedule.Subject subject = day.getSubjects().get(position);

        holder.beginTimeTextView.setText(subject.getBeginLessonString());
        holder.endTimeTextView.setText(subject.getEndLessonString());
        holder.lecturerTextView.setText(subject.getLecturer());
        holder.auditoriumTextView.setText(subject.getAuditorium());
        holder.lessonTypeTextView.setText(subject.getKindOfWork());
        holder.buildingTextView.setText(subject.getBuilding());
        holder.lessonNameTextView.setText(subject.getDiscipline());
    }

    @Override
    public int getItemCount() {
        return day.getSubjects().size();
    }

    class SubjectViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{

        private TextView beginTimeTextView;
        private TextView endTimeTextView;
        private TextView lecturerTextView;
        private TextView auditoriumTextView;
        private TextView lessonTypeTextView;
        private TextView buildingTextView;
        private TextView lessonNameTextView;

        SubjectViewHolder(View itemView) {
            super(itemView);

            beginTimeTextView = (TextView) itemView.findViewById(R.id.begin_time);
            endTimeTextView = (TextView) itemView.findViewById(R.id.end_time);
            lecturerTextView = (TextView) itemView.findViewById(R.id.lecturer);
            auditoriumTextView = (TextView) itemView.findViewById(R.id.auditorium);
            lessonTypeTextView = (TextView) itemView.findViewById(R.id.lesson_type);
            buildingTextView = (TextView) itemView.findViewById(R.id.building);
            lessonNameTextView = (TextView) itemView.findViewById(R.id.lesson_name);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        //click - show time pick dialog to set alarm
        @Override
        public void onClick(View v) {
//            try {

            int position = getAdapterPosition();
            final Context context = v.getContext();
            if (position != RecyclerView.NO_POSITION) {
                final DaySchedule.Subject subject = day.getSubjects().get(position);

                int alarmOffset = PreferenceManager.getDefaultSharedPreferences(context)
                        .getInt(ALARM_OFFSET_PREFERENCE_PREF, 60);
                LocalTime lessonStart = subject.getBeginLesson();
                LocalTime alarmTime = lessonStart.minusMinutes(alarmOffset);
                String clockType = android.provider.Settings.System.getString(context.getContentResolver(), android.provider.Settings.System.TIME_12_24);
                final String alarmDescription = String.format(context.getString(R.string.alarm_description), subject.getDiscipline());
                TimePickerDialog alarmSetDialog = new TimePickerDialog(context,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker timePicker, int hour, int minute) {

                                setAlarm(context, hour, minute, alarmDescription);
                            }
                        },
                        alarmTime.getHourOfDay(),
                        alarmTime.getMinuteOfHour(),
                        DateFormat.is24HourFormat(context) || (clockType != null && clockType.equals("24")));
                alarmSetDialog.show();
                alarmSetDialog.getButton(AlertDialog.BUTTON_POSITIVE).setText(R.string.set_alarm_button_text);
            }
//            } catch (Exception e ) {
//                Toast.makeText(v.getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
//            }
        }

        //long click - immediate alarm
        @Override
        public boolean onLongClick(View view) {
            int position = getAdapterPosition();
            final Context context = view.getContext();
            boolean longClickAlarm = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(LONG_CLICK_SETS_ALARM, false);

            if (position != RecyclerView.NO_POSITION && longClickAlarm) {
                final DaySchedule.Subject subject = day.getSubjects().get(position);

                int alarmOffset = PreferenceManager.getDefaultSharedPreferences(context)
                        .getInt(ALARM_OFFSET_PREFERENCE_PREF, 60);
                LocalTime lessonStart = subject.getBeginLesson();
                LocalTime alarmTime = lessonStart.minusMinutes(alarmOffset);
                String alarmDescription = String.format(context.getString(R.string.alarm_description), subject.getDiscipline());
                setAlarm(context, alarmTime.getHourOfDay(), alarmTime.getMinuteOfHour(), alarmDescription);
                return true;
            }
            return false;
        }

        private void setAlarm(Context context, int hour, int minute, String description) {
            Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);
            intent.putExtra(AlarmClock.EXTRA_HOUR, hour);
            intent.putExtra(AlarmClock.EXTRA_MINUTES, minute);
            intent.putExtra(AlarmClock.EXTRA_MESSAGE, description);
            intent.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
            context.startActivity(intent);
        }
    }
}
